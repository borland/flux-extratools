FROM docker.io/fluxcd/flux:1.20.0
RUN /sbin/apk update ;\
    /sbin/apk add py3-pip;\
    pip install awscli
RUN cd /tmp ;\
    wget https://get.helm.sh/helm-v3.1.3-linux-amd64.tar.gz -O helm.tgz ;\
    tar -xvf helm.tgz ;\
    mv /tmp/linux-amd64/helm /usr/local/bin/helm ;\
    chmod a+x /usr/local/bin/helm ;\
    rm -Rf /tmp/helm*
RUN wget https://github.com/mozilla/sops/releases/download/v3.6.0/sops-v3.6.0.linux -O /usr/local/bin/sops ;\
    chmod a+x /usr/local/bin/sops
RUN helm plugin install https://github.com/zendesk/helm-secrets
